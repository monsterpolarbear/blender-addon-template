# Basic Blender Addon Template
* UI elements in `bl_panel.py` are connected to the operators in `bl_op.py` via `bl_idname`
* `__init__.py` imports and registers everything.
* `bl_prefs.py` adds stuff to preferences

## Dev
* symlink folder 'addonTemplate' into /home/user/.config/blender/version/scripts/addons

## Release
* zip 'addonTemplate', install in prefs by picking zip-file