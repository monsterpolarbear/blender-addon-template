# -*- coding: utf-8 -*-
import bpy
import json

from .logic import do_stuff


class TEMPLATE_OT_DO_STUFF(bpy.types.Operator):
    bl_idname ='view3d.do_stuff'
    bl_label = 'Addon Template do stuff'
    bl_description = 'Addon Template do stuff'
    def execute(self, context):
        do_stuff()
        
        return {'FINISHED'}