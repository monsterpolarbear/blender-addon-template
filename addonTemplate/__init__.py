# -*- coding: utf-8 -*-
bl_info = {
    'name': 'addonTemplate',
    'author': '',
    'description': 'Simple Addon Template',
    'blender': (2, 80, 0),
    'location': 'View3D',
    'warning': '',
    'category': ''
}

import bpy
from .bl_op import TEMPLATE_OT_DO_STUFF
from .bl_panel import TEMPLATE_PT_Panel
from .bl_prefs import addonTemplatePrefs

classes = (TEMPLATE_OT_DO_STUFF,TEMPLATE_PT_Panel,addonTemplatePrefs)


# register, unregister = bpy.utils.register_classes_factory(classes)

def register():

    from bpy.utils import register_class
    for cls in classes:
        try:
            register_class(cls)
            print(f"Registering {cls.__name__}")
        except:
            print(f"{cls.__name__} already registered")
    print("Registered Addon Template")
    

def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        try:
            unregister_class(cls)
        except:
            print(f"{cls.__name__} already unregistered")
    print("Unregistered Addon Template")
    del (bpy.types.Scene.queryProps)
