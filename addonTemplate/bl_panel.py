# -*- coding: utf-8 -*-
import bpy

class TEMPLATE_PT_Panel(bpy.types.Panel):
    bl_idname = 'TEMPLATE_PT_Panel'
    bl_label = 'addonTemplate'
    bl_category = 'addonTemplate'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI' #sidebar on the right (N key)

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        objs = scene.objects
        props = bpy.context.scene.queryProps

        col = layout.column(align=True)
        rowsub = col.row(align=True)  # make new row
        rowsub.label(text="Addon Template")  # add to row

        rowsub = col.row(align=True)  # make new row
        rowsub.operator('view3d.do_stuff', text='Print Something', icon = 'EMPTY_AXIS')
 
        
        
