import bpy
import time

def create_timestamp():
    timestr = time.strftime("%Y_%m_%d__%H%M%S")
    return timestr


def do_stuff():
    dir_from_prefs = bpy.context.preferences.addons[__package__].preferences.some_dir
    print(f'{create_timestamp()}:   {dir_from_prefs}')