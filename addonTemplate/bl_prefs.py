# -*- coding: utf-8 -*-

import bpy


class addonTemplatePrefs(bpy.types.AddonPreferences):
    bl_idname = __package__

    some_dir: bpy.props.StringProperty(name='Some Directory', subtype='DIR_PATH',
            description="Some directory",
            default='/home/user/dev',
            update=None,
        )

    def draw(self, context):
        layout = self.layout
        layout.label(text='Blender Addone Template')
        row = layout.row()
        row.prop(self, 'some_dir', expand=True)